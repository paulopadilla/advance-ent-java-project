package com.entjava.exceptions;

public class InvalidPasswordConfirmation extends Exception implements Messages {

	public InvalidPasswordConfirmation() {
		super(INVALID_PASSWORD);
	}
	
	public InvalidPasswordConfirmation(String message){
		super(message);
	}
}
