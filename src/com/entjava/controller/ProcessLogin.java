package com.entjava.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jasypt.util.password.StrongPasswordEncryptor;

import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;

import com.entjava.exceptions.InvalidCaptchaErr;
import com.entjava.exceptions.InvalidLogin;
import com.entjava.utility.BeanAssembler;
import com.entjava.utility.UserConnector;

@WebServlet("/processlogin.html")
public class ProcessLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		Captcha captcha = (Captcha) session.getAttribute(Captcha.NAME);
		AudioCaptcha audCaptcha = (AudioCaptcha) session.getAttribute(AudioCaptcha.NAME);

		request.setCharacterEncoding("UTF-8");
		String answer = request.getParameter("answer");

		try {
			if (captcha.isCorrect(answer) || audCaptcha.isCorrect(answer)) {
				System.out.println("Right");
				
				UserConnector userConn = new UserConnector();
				// sd.test();
				ResultSet rs = userConn.checkLogin(request.getParameter("emailAdd"),
												   decryptUserPassword(request.getParameter("userPassword"), 
														   			   request.getParameter("emailAdd"), userConn));
				
				if (rs.next()) {
					System.out.println(rs.getString("user_firstname"));
					System.out.println(rs.getString("user_middlename"));
					System.out.println(rs.getString("user_lastname"));
					System.out.println(rs.getString("user_contact"));
					System.out.println(rs.getString("user_email"));
					System.out.println(rs.getDate("user_dob"));
					session.setAttribute("loggedUser", BeanAssembler.assembleUser(rs.getString("user_firstname"),
																				  rs.getString("user_lastname"), 
																				  rs.getString("user_middlename"), 
																				  rs.getString("user_email"),
																				  rs.getString("user_contact"),
																				  rs.getTimestamp("user_lastlogin"),
																				  rs.getDate("user_joined")));
					session.setMaxInactiveInterval(120);
				} else {
					throw new InvalidLogin();
				}
			} else {
				throw new InvalidCaptchaErr();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} catch (InvalidLogin ile) {
			ile.printStackTrace();
		} catch (InvalidCaptchaErr ice) {
			ice.printStackTrace();
		}
		request.getRequestDispatcher("home.jsp").forward(request, response);
	}
	
	private String decryptUserPassword(String userPassword, String emailAdd, UserConnector userConn) throws SQLException{
		StrongPasswordEncryptor passwordDecrypt = new StrongPasswordEncryptor();
		String encryptedPass = userConn.getEncryptedPassword(emailAdd);
		if(passwordDecrypt.checkPassword(userPassword, encryptedPass)){
			return encryptedPass;
		}else{
			return null;
		}
	}

}
