<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>

</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<%@ include file="styles/navbar.jsp"%>
		 </div>
		<div class="container marketing">
			<!-- Companies -->
			<div class="row">
				<div class="col-lg-4">
					<img class="img" src="images/meralco.jpg"
						alt="Generic placeholder image"
						style="width: 230px; height: 200px;">
					<h2>Meralco</h2>
					<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis
						euismod. Nullam id dolor id nibh ultricies vehicula ut id elit.
						Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
						Praesent commodo cursus magna.</p>
					<p>
						<a class="btn btn-default" href="#" role="button">View details
							&raquo;</a>
					</p>
				</div>
				<!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="img" src="images/manila_water.jpeg"
						alt="Generic placeholder image"
						style="width: 250px; height: 250px;">
					<h2>Manila Water</h2>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor
						ligula, eget lacinia odio sem nec elit. Cras mattis consectetur
						purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo,
						tortor mauris condimentum nibh.</p>
					<p>
						<a class="btn btn-default" href="#" role="button">View details
							&raquo;</a>
					</p>
				</div>
				<!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="img" src="images/pldt.png"
						alt="Generic placeholder image"
						style="width: 400px; height: 180px;">
					<h2>PLDT</h2>
					<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis
						in, egestas eget quam. Vestibulum id ligula porta felis euismod
						semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris
						condimentum nibh, ut fermentum massa justo sit amet risus.</p>
					<p>
						<a class="btn btn-default" href="#" role="button">View details
							&raquo;</a>
					</p>
				</div>
				<!-- /.col-lg-4 -->
			</div>
			<!-- /.row -->

		</div>
		<!-- marketing /.container -->
		<%@ include file="styles/footer.html"%>
	</div>
	<!-- /container -->
	<%@ include file="styles/includejs.html"%>
</body>
</html>