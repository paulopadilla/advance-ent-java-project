<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>
<link href="css/simple-sidebar.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<%@ include file="styles/navbar.jsp"%>
		</div>
	</div>
	<div id="wrapper">

		<!-- ******************** SIDEBAR **************************** -->
		<%@ include file="styles/sidebar.jsp"%>


		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>Transaction History</h1>
						<br> <br>
					</div>
					<!-- Table -->
					<div class="scrollable">
						<table class="table table-striped">
							<tr>
								<th>No.</th>
								<th>Payment Date</th>
								<th>Service Provider</th>
								<th>Payment Mode</th>
								<th>Amount</th>
								<th></th>
							</tr>
							<tr>
								<td>1</td>
								<td>12/12/12</td>
								<td>PLDT</td>
								<td>Paypal</td>
								<td>5000</td>
							</tr>

						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="styles/footer.html"%>

	<!-- /container -->
	<%@ include file="styles/includejs.html"%>
</body>
</html>