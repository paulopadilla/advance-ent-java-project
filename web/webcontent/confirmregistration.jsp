<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<style>
.col-centered{
    float: none;
    margin: 0 auto;
}
</style>

<%@ include file="//styles/includecss.html"%>

<title>BayadSintir</title>

<style>
.col-centered{
    float: none;
    margin: 0 auto;
}
</style>
</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<%@ include file="//styles/navbar.jsp"%>
		</div>
	</div>


	<!-- ************************ INFO COLUMNS *************************** -->
	
	<div class="col-md-4 col-centered">
		<ul class="list-group">
			<li class="list-group-item text-info text-center lead">Profile Details</li>
			
			<li class="list-group-item text-right"><span class="pull-left"><strong>Last Name:
				</strong></span> ${userRegistrant.lastName}</li>
			<li class="list-group-item text-right"><span class="pull-left"><strong>First Name:
				</strong></span> ${userRegistrant.firstName}</li>
			<li class="list-group-item text-right"><span class="pull-left"><strong>Middle Name:
				</strong></span> ${userRegistrant.middleName}</li>
			<li class="list-group-item text-right"><span class="pull-left"><strong>Email Address:
				</strong></span> ${userRegistrant.emailAdd}</li>
			<li class="list-group-item text-right"><span class="pull-left"><strong>Contact No.:
				</strong></span> ${userRegistrant.contactNo}</li>
			<li class="list-group-item text-right"><span class="pull-left"><strong>Date of Birth:
				</strong></span> ${userRegistrant.dateOfBirth}</li>
			<li class="list-group-item text-right">

				<a class="btn btn-success btn-block" type="submit" href="confirmregistration.html">Confirm</a>
			</li>
		</ul>
	</div>

	<%@ include file="//styles/footer.html"%>
	</div>
	<!-- /container -->

	<%@ include file="//styles/includejs.html"%>


</body>
</html>