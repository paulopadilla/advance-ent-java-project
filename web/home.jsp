<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">


<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>

<style>
.jumbotron {
	margin-bottom: 0px;
	background-image: url(images/poster.jpg);
	background-position: 0%;
	background-size: cover;
	background-repeat: no-repeat;
	color: red;
	text-shadow: black 0.2em 0.2em 0.2em;
}
</style>
</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<%@ include file="styles/navbar.jsp"%>
		</div>
	</div>
	<div class="container">

		<!-- ********************* JUMBOTRON ****************************  -->
		<div class="jumbotron poster">
			<!--  <img src="images/poster.jpg">-->
			<h1>
				<br> <br> <br> <br>
			</h1>
			<p class="lead">All your bills just a click away!</p>

		</div>

		<!-- ********************** END OF JUMBOTRON ******************** -->

		<!-- ************************ INFO COLUMNS *************************** -->
		<div class="row">
			<div class="col-lg-4">
				<h2>Paypal</h2>
				<img class="img" src="images/paypal.jpg"
					alt="Generic placeholder image"
					style="width: 350px; height: 180px;"><br> <br>
				<p class="text-danger">As of v7.0.1, Safari exhibits a bug in
					which resizing your browser horizontally causes rendering errors in
					the justified nav that are cleared upon refreshing.</p>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus. Etiam porta sem malesuada
					magna mollis euismod. Donec sed odio dui.</p>
				<p>
					<a class="btn btn-primary" href="#" role="button">View details
						&raquo;</a>
				</p>
			</div>
			<div class="col-lg-4">
				<h2>Credit Card</h2>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus. Etiam porta sem malesuada
					magna mollis euismod. Donec sed odio dui.</p>
				<p>
					<a class="btn btn-primary" href="#" role="button">View details
						&raquo;</a>
				</p>
			</div>

		<%@include file="styles/registrationhome.jsp" %>
		</div>

		<%@ include file="styles/footer.html"%>
	</div>
	<!-- /container -->

	<%@ include file="styles/includejs.html"%>


</body>
</html>