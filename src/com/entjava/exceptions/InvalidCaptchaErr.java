package com.entjava.exceptions;

public class InvalidCaptchaErr extends Exception implements Messages {
	
	public InvalidCaptchaErr(){
		super(INVALID_CAPTCHA);
	}
	
	public InvalidCaptchaErr(String message){
		super(message);
	}
}
