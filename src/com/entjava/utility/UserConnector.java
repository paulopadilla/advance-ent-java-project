package com.entjava.utility;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import com.entjava.model.User;
import com.mysql.jdbc.PreparedStatement;

public class UserConnector {
	PreparedStatement pstmt;
	ResultSet rs;

	public void test() {
		String sql;
		try {

			Connection conn = DatabaseSingleton.getInstance().getConnection();
			Statement statement = conn.createStatement();
			sql = "SELECT user_email FROM `adv-entjava`.user;";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				System.out.println(resultSet.getString("user_email"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ResultSet checkLogin(String emailAdd, String userPassword)
			throws SQLException {

		pstmt = (PreparedStatement) DatabaseSingleton
				.getInstance()
				.getConnection()
				.prepareStatement(
						"SELECT * FROM `adv-entjava`.user WHERE user_email = ? AND user_password = ?");
		pstmt.setString(1, emailAdd);
		pstmt.setString(2, userPassword);

		return pstmt.executeQuery();
	}

	public String getEncryptedPassword(String emailAdd) throws SQLException {
		pstmt = (PreparedStatement) DatabaseSingleton
				.getInstance()
				.getConnection()
				.prepareStatement(
						"SELECT * FROM `adv-entjava`.user WHERE user_email = ?");
		pstmt.setString(1, emailAdd);
		System.out.println("Getting encrypted password");
		rs = pstmt.executeQuery();
		if (rs.next()) {
			System.out.println(rs.getString("user_password"));
			return rs.getString("user_password");
		}
		return null;
	}

	public void registerAccount(User user) throws SQLException {
		long time = System.currentTimeMillis();
		Date date = new Date(time);
		pstmt = (PreparedStatement) DatabaseSingleton
				.getInstance()
				.getConnection()
				.prepareStatement(
						"INSERT INTO `adv-entjava`.`user` (`user_email`,"
								+ " `user_password`, `user_contact`, `user_firstname`,"
								+ " `user_lastname`, `user_middlename`, `user_dob`, `user_joined`) "
								+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		pstmt.setString(1, user.getEmailAdd());
		pstmt.setString(2, user.getUserPassword());
		pstmt.setString(3, user.getContactNo());
		pstmt.setString(4, user.getFirstName());
		pstmt.setString(5, user.getLastName());
		pstmt.setString(6, user.getMiddleName());
		pstmt.setDate(7, Date.valueOf(user.getDateOfBirth()));
		pstmt.setDate(8, date);
		pstmt.executeUpdate();
	}

	public void updateLogoutTime(User user) throws SQLException {
		long time = System.currentTimeMillis();
		Timestamp dateTime = new Timestamp(time);

		pstmt = (PreparedStatement) DatabaseSingleton
				.getInstance()
				.getConnection()
				.prepareStatement(
						"UPDATE `adv-entjava`.`user` SET `user_lastlogin`=? WHERE `user_email`=?");
		pstmt.setTimestamp(1, dateTime);
		pstmt.setString(2, user.getEmailAdd());
		pstmt.executeUpdate();
	}

	public boolean checkUserExists(String emailAdd) {
		try {
			pstmt = (PreparedStatement) DatabaseSingleton
					.getInstance()
					.getConnection()
					.prepareStatement(
							"SELECT user_email FROM `adv-entjava`.user WHERE user_email = ?");
			pstmt.setString(1, emailAdd);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public int getUserID(String emailAdd) throws SQLException {
		pstmt = (PreparedStatement) DatabaseSingleton
				.getInstance()
				.getConnection()
				.prepareStatement(
						"SELECT user_id FROM `adv-entjava`.user WHERE user_email = ?");
		pstmt.setString(1, emailAdd);
		rs = pstmt.executeQuery();
		
		if(rs.next()){
			return rs.getInt("user_id");
		}
		return 0;
	}
}
