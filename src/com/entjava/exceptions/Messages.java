package com.entjava.exceptions;

public interface Messages {
	String INVALID_LOGIN = "User doesn't exists or Wrong email/password.";
	String INVALID_CAPTCHA = "Wrong Captcha";
	String INVALID_AGE = "Should be atleast 18 years old.";
	String INVALID_PASSWORD = "Password does not match.";
	
	String FAILED_REGISTRATION = "Registration failed.";
	String USER_EXISTS = "User exists.";
}
