
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed"
			data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#"></a>
	</div>

	<!-- ********************* NAVBAR CONTENTS ************************ -->

	<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li><a href="home.jsp"><span
					class="glyphicon glyphicon-home"></span> Home </a></li>
			<li><a href="howto.jsp"><span
					class="glyphicon glyphicon-question-sign"></span> How To</a></li>
			<li><a href="partners.jsp"><span
					class="glyphicon glyphicon-briefcase"></span> Partners</a></li>

			<li><a href="about.jsp"><span
					class="glyphicon glyphicon-info-sign"></span> About</a></li>
		</ul>
		<jsp:include page="/checklogin.html"></jsp:include>
	</div>

</div>
<!-- ******************** END OF NAVBAR *********************** -->