package com.entjava.exceptions;

public class FailedRegistration extends Exception implements Messages {

	public FailedRegistration(){
		super(FAILED_REGISTRATION);
	}
	
	public FailedRegistration(String message){
		super(message);
	}
	
}
