<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>
<link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<%@ include file="styles/navbar.jsp"%>
		</div>
	</div>
	<div id="wrapper">

		<!-- ******************** SIDEBAR **************************** -->
		<%@ include file="styles/sidebar.jsp"%>


		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>Manage Bills</h1>
						<br> <br>
					</div>
				</div>
				<div class="col-sm-4">
					<ul class="list-group">
						<li class="list-group-item text-info lead">Meralco</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Date started: </strong></span>12/12/12</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Total Payments: </strong></span>54</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Latest date of payment:
							</strong></span> Yesterday</li>
					</ul>
					<button type="button" class="btn btn-danger btn-block">
						Remove <span class="glyphicon glyphicon-remove"></span>
					</button>
				</div>
				<div class="col-sm-4">
					<ul class="list-group">
						<li class="list-group-item text-info lead">PLDT</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Date started: </strong></span>12/12/12</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Total Payments: </strong></span>54</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Latest date of payment:
							</strong></span> Yesterday</li>
					</ul>
					<button type="button" class="btn btn-danger btn-block">
						Remove <span class="glyphicon glyphicon-remove"></span>
					</button>
				</div>
				<div class="col-sm-4">
					<ul class="list-group">
						<li class="list-group-item text-info lead">Manila Water</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Date started: </strong></span>12/12/12</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Total Payments: </strong></span>54</li>
						<li class="list-group-item text-right"><span
							class="pull-left"><strong>Latest date of payment:
							</strong></span> Yesterday</li>
					</ul>
					<button type="button" class="btn btn-danger btn-block">
						Remove <span class="glyphicon glyphicon-remove"></span>
					</button>
				</div>
			</div>
		</div>

		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="styles/footer.html"%>

	<!-- /container -->
	<%@ include file="styles/includejs.html"%>


</body>
</html>