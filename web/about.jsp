<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>

<style>
.jumbotron {
	margin-bottom: 0px;
	background-image: url(images/poster.jpg);
	background-position: 0%;
	background-size: cover;
	background-repeat: no-repeat;
	color: red;
	text-shadow: black 0.2em 0.2em 0.2em;
}
</style>
</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<%@ include file="styles/navbar.jsp"%>
		 </div>
			<!-- ********************* JUMBOTRON ****************************  -->
			<div class="jumbotron poster">
				<!--  <img src="images/poster.jpg">-->
				<h1>
					<br> <br> <br> <br>
				</h1>
				<p class="lead">All your bills just a click away!</p>

			</div>

			<!-- ********************** END OF JUMBOTRON ******************** -->
		</div>

		<!-- ************************ INFO COLUMNS *************************** -->
		<div class="row">

			<div class=" text-center">
				<h2>Bayad Sintir</h2>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus. Etiam porta sem malesuada
					magna mollis euismod. Donec sed odio dui.</p>
				<p>
					<a class="btn btn-primary" href="#" role="button">View details
						&raquo;</a>
				</p><br><br>
			</div>
			<div class="[ col-sm-6 col-md-offset-2 col-md-4 ]">
				<div class="[ info-card ]">
					<img style="width: 100%" src="images/paulo.jpg" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1>Jose Paulo Padilla</h1>
							<h3>aka Peanut</h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Ait enim se, si uratur, Quam hoc suave! dicturum. Atque his de
								rebus et splendida est eorum et illustris</p>
							<div class="social">
								<a href="https://www.facebook.com/paulopadilla04?"
									class="[ social-icon facebook ] animate"><span
									class="fa fa-facebook"></span></a> <a href=""
									class="[ social-icon twitter ] animate"><span
									class="fa fa-twitter"></span></a>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="[ col-sm-6 col-md-4 ]">
				<div class="[ info-card ]">
					<img style="width: 100%" src="images/eryca.jpg" />
					<div class="[ info-card-details ] animate">
						<div class="[ info-card-header ]">
							<h1>Eryca Jennel Basa</h1>
							<h3>aka not a real person</h3>
						</div>
						<div class="[ info-card-detail ]">
							<!-- Description -->
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Ait enim se, si uratur, Quam hoc suave! dicturum. Atque his de
								rebus et splendida est eorum et illustris.</p>
							<div class="social">
								<a href="https://www.facebook.com/eryco"
									class="[ social-icon facebook ] animate"><span
									class="fa fa-facebook"></span></a> <a
									href="https://twitter.com/_eRaYKa"
									class="[ social-icon twitter ] animate"><span
									class="fa fa-twitter"></span></a>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<%@ include file="styles/footer.html"%>
	</div>
	<!-- /container -->

	<%@ include file="styles/includejs.html"%>


</body>
</html>