package com.entjava.utility;

import java.sql.Date;
import java.sql.Timestamp;

import com.entjava.model.User;

public class BeanAssembler {
	
	public static User assembleUser (String firstName,
								  	 String lastName,
								  	 String middleName,
								  	 String emailAdd,
								  	 String contactNo,
								  	 String dateOfBirth,
								  	 String userPassword){
		
		User assembledUser = new User();
		assembledUser.setFirstName(firstName);
		assembledUser.setLastName(lastName);
		assembledUser.setMiddleName(middleName);
		assembledUser.setEmailAdd(emailAdd);
		assembledUser.setContactNo(contactNo);
		assembledUser.setDateOfBirth(dateOfBirth);
		assembledUser.setUserPassword(userPassword);
		
		return assembledUser;
	}
	
	public static User assembleUser (String firstName,
				  	 String lastName,
				  	 String middleName,
				  	 String emailAdd,
				  	 String contactNo,
				  	 Timestamp lastLogged,
				  	 Date dateJoined){
		
		User assembledUser = new User();
		assembledUser.setFirstName(firstName);
		assembledUser.setLastName(lastName);
		assembledUser.setMiddleName(middleName);
		assembledUser.setEmailAdd(emailAdd);
		assembledUser.setContactNo(contactNo);
		assembledUser.setLastLogged(lastLogged);
		assembledUser.setDateJoined(dateJoined);
		return assembledUser;
	}
			
	
}
