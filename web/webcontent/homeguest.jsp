
<!-- *********************** SIGN UP **************************** -->
<div class="col-lg-4">
	<h2>Not Yet A Member?</h2>
	<h2>Sign Up Now!</h2>
	<form class="form" role="form" method="post"
		action="processregistration.html" accept-charset="UTF-8">
		<input type="text" class="form-control" placeholder="Last Name"
			id="lastName" name="lastName" required autofocus> <br> <input
			type="text" class="form-control" placeholder="First Name"
			id="firstName" name="firstName" required> <br> <input
			type="text" class="form-control" placeholder="Middle Name"
			id="middleName" name="middleName" required> <br> <input
			type="date" class="form-control" placeholder="Birthday"
			id="dateOfBirth" name="dateOfBirth" required> <br> <input
			type="text" class="form-control" placeholder="Mobile No. 09123456789"
			maxlength="13" id="contactNo" name="contactNo" required> <br>
		<input type="email" class="form-control" id="emailAdd" name="emailAdd"
			placeholder="you@example.com" required> <br> <input
			type="password" class="form-control" id="userPassword"
			name="userPassword" placeholder="Password" required> <br>
		<input type="password" class="form-control" id="userPassword2"
			name="userPassword2" placeholder="Password" required> <br>
		<center>
			<img src="/bayadsintir/simpleCaptcha" /> <br> or <br>
			<audio controls>
				<source src="/bayadsintir/audio.wav" type="audio/wav">
			</audio>
			<br> <br>
		</center>
		<input name="answer" class="form-control" placeholder="Captcha"
			required /><br>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
			Up</button>
	</form>
</div>