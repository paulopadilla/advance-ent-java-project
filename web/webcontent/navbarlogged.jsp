<ul class="nav navbar-nav navbar-right">

	<li class="dropdown"><a href="#" class="dropdown-toggle"
		data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>
			Welcome, ${loggedUser.firstName } <b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li>
				<a href="useraccount.jsp">
					<span class="glyphicon glyphicon-user"></span> Account
				</a>
			</li>
			<li>
				<a href="mywallet.jsp">
					<span class="glyphicon glyphicon-credit-card"></span> Wallet Balance - 
					<span class="badge">5000</span>
				</a>
			</li>
			<li class="divider"></li>
			<li><a href="logoutuser.html">
				<span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

		</ul></li>
</ul>