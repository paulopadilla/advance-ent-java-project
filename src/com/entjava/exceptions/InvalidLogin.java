package com.entjava.exceptions;

public class InvalidLogin extends Exception implements Messages{
	
	public InvalidLogin(){
		super(INVALID_LOGIN);
	}
	
	public InvalidLogin(String message){
		super(message);
	}
}
