package com.entjava.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.entjava.model.User;
import com.entjava.utility.UserConnector;

@WebServlet("/logoutuser.html")
public class ProcessLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User userLogout = (User)session.getAttribute("loggedUser");
		UserConnector userConn = new UserConnector();
		try {
			userConn.updateLogoutTime(userLogout);
		} catch (SQLException sqle) {
			System.out.println("Failed to update Last logged in time");
		}
		session.invalidate();
		response.sendRedirect("home.jsp");
	}

}
