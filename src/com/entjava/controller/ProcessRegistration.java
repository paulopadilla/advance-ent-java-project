package com.entjava.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;

import org.jasypt.util.password.StrongPasswordEncryptor;

import com.entjava.exceptions.InvalidCaptchaErr;
import com.entjava.exceptions.InvalidPasswordConfirmation;
import com.entjava.exceptions.UserExists;
import com.entjava.model.User;
import com.entjava.utility.BeanAssembler;
import com.entjava.utility.UserConnector;

@WebServlet("/processregistration.html")
public class ProcessRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		Captcha imgCaptcha = (Captcha) session.getAttribute(Captcha.NAME);
		AudioCaptcha audCaptcha = (AudioCaptcha) session.getAttribute(AudioCaptcha.NAME);

		request.setCharacterEncoding("UTF-8");
		String answer = request.getParameter("answer");
		UserConnector userConn = new UserConnector();
		try {
			if (imgCaptcha.isCorrect(answer) || audCaptcha.isCorrect(answer)) {
				if(request.getParameter("userPassword").equals(request.getParameter("userPassword2"))){
					if(!userConn.checkUserExists(request.getParameter("emailAdd"))){
						User userRegistrant = BeanAssembler.assembleUser(request.getParameter("firstName"),
								 request.getParameter("lastName"),
								 request.getParameter("middleName"),
								 request.getParameter("emailAdd"),
								 request.getParameter("contactNo"),
								 request.getParameter("dateOfBirth"),
								 encryptUserPassword(request.getParameter("userPassword")));
						System.out.println( encryptUserPassword(request.getParameter("userPassword")));
						session.setAttribute("userRegistrant", userRegistrant);
						request.getRequestDispatcher("/webcontent/confirmregistration.jsp").forward(request, response);
					}else{
						throw new UserExists();
					}
				}else{
					throw new InvalidPasswordConfirmation();
				}
			} else {
				throw new InvalidCaptchaErr();
			}
		} catch (InvalidCaptchaErr ice) {
			
		} catch (InvalidPasswordConfirmation ipce) {

		} catch (UserExists ue) {
			
		}
	}
	
	private String encryptUserPassword(String userPassword){
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		return passwordEncryptor.encryptPassword(userPassword);
	}
}
