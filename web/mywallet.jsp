<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>
<link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<%@ include file="styles/navbar.jsp"%>
		</div>
	</div>
	<div id="wrapper">

		<!-- ******************** SIDEBAR **************************** -->
		<%@ include file="styles/sidebar.jsp"%>


		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>My Wallet</h1>
						<br>
						<br>
					</div>

				</div>
				<div class="col-sm-4">
					<ul class="list-group">
						<li class="list-group-item text-success lead">Your Wallet has</li>
						<li class="list-group-item"><strong>Php 5,000.00</strong></li>
					</ul>
				</div>
				<div class="col-sm-5">
					<a class=" btn-lg btn-primary btn-block" href="#" role="button">Load
						via Paypal &raquo;</a> <a class=" btn-lg btn-primary btn-block"
						href="#" role="button">Load via Credit Card &raquo;</a>
				</div>

				<!-- Table -->
				<div class="scrollable col-lg-12">
					<h1 class="text-left">Wallet History</h1>
					<br>
					<table class="table table-striped">
						<tr>
							<th>No.</th>
							<th>Date</th>
							<th>Action</th>
							<th>Mode of Payment</th>
							<th>Amount</th>
							<th>Balance</th>
						</tr>
						<tr>
							<td>1</td>
							<td>12/12/12</td>
							<td>Load</td>
							<td>Paypal</td>
							<td>5000</td>
							<td>5000</td>
						</tr>
						
					</table>
				</div>
			</div>
		</div>

		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="styles/footer.html"%>

	<!-- /container -->
	<%@ include file="styles/includejs.html"%>


</body>
</html>