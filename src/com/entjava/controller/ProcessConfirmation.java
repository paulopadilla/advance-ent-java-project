package com.entjava.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.entjava.model.User;
import com.entjava.utility.UserConnector;


@WebServlet("/confirmregistration.html")
public class ProcessConfirmation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserConnector userConn = new UserConnector();
		HttpSession session = request.getSession();
		if(session.getAttribute("userRegistrant") == null){
			System.out.println("null session object");
			response.sendRedirect("home.jsp");
		}else{
			try {
				userConn.registerAccount((User)session.getAttribute("userRegistrant"));
				session.removeAttribute("userRegistrant");
				response.sendRedirect("home.jsp");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
