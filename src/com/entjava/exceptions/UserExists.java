package com.entjava.exceptions;

public class UserExists extends Exception implements Messages {

	public UserExists(){
		super(USER_EXISTS);
	}
	
	public UserExists(String message){
		super(message);
	}
}
