package com.entjava.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseSingleton {
	private static DatabaseSingleton dbSingleton = new DatabaseSingleton();
	private static Connection connection;
	private static FileInputStream fis;
	private static Properties prop;
	static String propFile = "G:/IDEs/Java/My Workspace/Advance EntJava/SQL Properties.property";

	private DatabaseSingleton() {
		
	}

	public static DatabaseSingleton getInstance() {
		if (dbSingleton == null)
			dbSingleton = new DatabaseSingleton();
		return dbSingleton;
	}
	
	public Connection getConnection(){
		if(connection == null) initialize();
		return connection;
	}

	public void initialize() {
		try {
			System.out.println("dude singletong");
			fis = new FileInputStream(propFile);
			prop = new Properties();
			prop.load(fis);
			
			Class.forName(prop.getProperty("driver"));
			connection = DriverManager.getConnection("jdbc:mysql://" + 
							prop.getProperty("server") +"/"+ 
							prop.getProperty("database"),
							prop.getProperty("username"),
							prop.getProperty("password"));
		} catch (ClassNotFoundException e) {
			System.out.println("Connection Failed: " + e);
		} catch (SQLException e) {
			System.out.println("Connection Failed: " + e);
		} catch (IOException ioe) {
			System.err.println(ioe);
		}
	}

}
