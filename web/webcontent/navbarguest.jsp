<ul class="nav navbar-nav navbar-right">
	<li class="dropdown"><a href="#dropdown-menu"
		class="dropdown-toggle" data-toggle="dropdown"><span
			class="glyphicon glyphicon-log-in"></span> Sign in <b class="caret"></b></a>
		<ul class="dropdown-menu" style="padding: 15px; min-width: 250px;">
			<li>
				<div class="row">
					<div class="col-md-12">
						<form class="form" role="form" method="post"
							action="processlogin.html" accept-charset="UTF-8" id="login-nav">
							<div class="form-group">
								<label class="sr-only" for="exampleInputEmail2">Email
									address</label> <input type="email" class="form-control"
									id="exampleInputEmail2" name="emailAdd"
									placeholder="Email address" required>
							</div>
							<div class="form-group">
								<label class="sr-only" for="exampleInputPassword2">Password</label>
								<input type="password" class="form-control"
									id="exampleInputPassword2" name="userPassword"
									placeholder="Password" required>
							</div>
							<div class="checkbox">
								<label> <input type="checkbox"> Remember me
								</label>
							</div>

							<div class="form-group" align="center">

								<img src="/bayadsintir/simpleCaptcha" /> <br> or <br>
								<audio controls src="/bayadsintir/audio.wav" type="audio/wav">

								</audio>

								<br> <br>
								<input name="answer" class="form-control"
									placeholder="Image or Audio Captcha" required />
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-success btn-block">Sign
									in</button>
							</div>
						</form>
					</div>
				</div>
			</li>

		</ul></li>
</ul>