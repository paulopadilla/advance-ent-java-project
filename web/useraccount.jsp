<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="styles/includecss.html"%>

<title>BayadSintir</title>
<link href="css/simple-sidebar.css" rel="stylesheet">
</head>

<body>
	<div class="container">
		<!-- ******************** NAVBAR *************************** -->
		<div class="navbar navbar-default navbar-fixed-top" role="navigation">
			<%@ include file="styles/navbar.jsp"%>
		</div>
	</div>
	<div id="wrapper">

		<!-- ******************** SIDEBAR **************************** -->
		<%@ include file="styles/sidebar.jsp"%>


		<!-- Page Content -->
		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<h1>
							User Account &emsp;<span> <a class=" btn btn-success"
								href="#" role="button"> Edit Account <span
									class="glyphicon glyphicon-pencil"></span></a>
							</span>
						</h1>
						<br> <br>
					</div>

					<div class="col-md-4">
						<!--left col-->

						<ul class="list-group">
							<li class="list-group-item text-info lead">Profile</li>
							<li class="list-group-item text-right"><span
								class="pull-left"><strong>Name: </strong></span> ${loggedUser.firstName} ${loggedUser.middleName} ${loggedUser.lastName}</li>
							<li class="list-group-item text-right"><span
								class="pull-left"><strong>Joined: </strong></span> ${loggedUser.dateJoined}</li>
							<li class="list-group-item text-right"><span
								class="pull-left"><strong>Last logged in:</strong></span> ${loggedUser.lastLogged }</li>
						</ul>

						<ul class="list-group">
							<li class="list-group-item text-info lead">Enrolled Bills</li>
							<li class="list-group-item"><strong>Meralco</strong></li>
							<li class="list-group-item"><strong>PLDT</strong></li>
							<li class="list-group-item"><strong>Manila Water</strong></li>
						</ul>
					</div>
					<div class="col-md-4">
						<ul class="list-group">
							<li class="list-group-item text-info lead">Contact
								Information</li>
							<li class="list-group-item text-right"><span
								class="pull-left"><strong>Email: </strong></span>${loggedUser.emailAdd }</li>
							<li class="list-group-item text-right"><span
								class="pull-left"><strong>Mobile No.:</strong></span>${loggedUser.contactNo}</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<%@ include file="styles/footer.html"%>

	<!-- /container -->
	<%@ include file="styles/includejs.html"%>
</body>
</html>